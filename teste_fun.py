import unittest
import func


class FunTestCase(unittest.TestCase):
    def test_name_last(self):
        retorno = func.name_last('Maria', 'Silva')
        self.assertEqual(retorno, 'Maria Silva')

    def test_name_last_middle(self):
        retorno = func.name_last('Maria', 'Silva', 'Bonita')
        self.assertEqual(retorno, 'Maria Bonita Silva')



if __name__ == '__main__':
    unittest.main()

class Produto:
    def __init__(self, tipo, estoque={}):
        self.tipo = tipo
        self.estoque = estoque

    def tipo_produto(self):
        return self.tipo

    def produto_estoque(self, produto, quantidade):
        if produto in self.estoque.keys():
            if self.estoque[produto] >= quantidade:
                return True
            else:
                return False

        else:
            return False

    def fatura_produto(self, produto, quantidade):
        if self.produto_estoque(produto, quantidade):
            self.estoque[produto] = self.estoque[produto] - quantidade
            return self.estoque[produto]
        else:
            return -1

    def estoque_atual_produto(self, produto):
        if produto in self.estoque.keys():
            return self.estoque[produto]
        else:
            return -1
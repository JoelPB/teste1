import unittest

from produto import Produto

class ProdutoTestCase(unittest.TestCase):
    def setUp(self):
        produto_tipo = 'bebida'
        self.produto_quantidade_estoque = {'refrigerante': 3, 'aguardente': 1, 'cerveja': 4, 'leite': 6, 'água': 10}
        self.produto = Produto(produto_tipo, self.produto_quantidade_estoque)

    def test_produto(self):
        self.assertEqual(self.produto.tipo_produto(), 'bebida')

    def test_produto_estoque(self):
        self.assertEqual(self.produto.produto_estoque('refrigerante', 2), True)

    def test_produto_estoque_false(self):
        self.assertEqual(self.produto.produto_estoque('aguardente', 4), False)

    def test_produto_estoque_no(self):
        self.assertEqual(self.produto.produto_estoque('feijão', -1), False)

    def test_fatura_produto_estoque(self):
        self.assertEqual(self.produto.fatura_produto('refrigerante', 2), 1)

    def test_estoque_atual_produto(self):
        self.assertEqual(self.produto.estoque_atual_produto('refrigerante'), 3)


if __name__ == '__main__':
    unittest.main()

def name_last(name, last, middle=''):
    if middle:
        return name + " " + middle + " " + last
    else:
        return name + " " + last
